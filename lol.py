# class A:
#     def __init__(self, a):
#         self.a = A

#     def speak(self):
#         print(self.a)

# class B:
#     def __init__(self, b):
#         self.b = B

#     def speak(self):
#         print(self.b)

# class Child(A,B):
#     def __init__(self, a, b):
#         self.a = a
#         self.b = b


# def product_array(numbers):
#     products = []
#     for i in range(len(numbers)):
#         product = 1
#         for j in range(len(numbers)):
#             if j == i:
#                 continue
#             product *= numbers[j]
#         products.append(product)
#     return products

# def product_array(numbers):
#     product = 1
#     products = []
#     for num in numbers:
#         product *= num
#     for num in numbers:
#         products.append(product/num)
#     return products


# print(product_array([3,27,4,2]))

# def find_shortest(lists):
#     # your code here
#     if len(lists) == 0:
#         return None
#     return min(lists, key = len)


# def total_revenue(product_sales):
#     # your code here
#     total = 0
#     for product in product_sales:
#         total += product['sales'] * product['price']
#     return total


# def total_revenue(product_sales):
#     # your code here
#    return sum([product['sales'] * product['price'] for product in product_sales])


# products_sold = [
#     { "sales": 11, "price": 9,   },
#     { "sales": 5,  "price": 1.5, },
# ]

# print(total_revenue(products_sold))

# def count_matches(items, match=None):
#     matches = []
#     for item in items:
#         if(item.get("background") == match):
#             matches.append(item)
#     return len(matches)

# input = [
#     {"background": "green",  "color": "blue", "size": 5},
#     {"color": "green", "size": 5, "weight": "heavy"},
#     {"background": "yellow", "size": 5,  "weight": "light"},
#     {"color": "blue", "size": 25, "weight": "light"},
# ]

# result = count_matches(input)
# print(result)

# from datetime import date
# class Invoice:
#     def __init__(self, customer_name, amount, invoice_date):
#         self.customer_name = customer_name
#         self.amount_due = amount
#         self.invoice_date = invoice_date

# def amount_due(invoices, grace_period_days=30):
#     past_due_amount = 0
#     for invoice in invoices:
#         days_past_due = (date.today() - invoice.invoice_date).days
#         if days_past_due > grace_period_days:
#             past_due_amount += invoice.amount_due
#     return past_due_amount

# invoices = [
#     Invoice("Anne", 10.00, date(2025, 6, 17)),
#     Invoice("Don", 75.00, date(2020, 7, 21)),
#     Invoice("Poli", 13.00, date(2029, 11, 5)),
#     Invoice("Raul", 60.00, date(2021, 4, 15)),
# ]

# result = amount_due(invoices)
# print(result)

# def make_chunks(list, size):
#     chunks = []
#     chunk = []
#     for item in list:
#         if len(chunk) == size:
#             chunks.append(chunk)
#             chunk = []
#         chunk.append(item)
#     return chunks

# input = [10, 7, 3, 11, 4]

# result = make_chunks(input, 3)
# print(result)

# dictionary = {
#     "dog": 1,
#     "number": "three",
#     "size": "2",
#     "heavy": True,
#     "weight": 3.4,
# }



# def sum_summables(dictionary):
#     total = 0

#     # loop over the things, get the ones you want
#     for value in dictionary.values():
#         if isinstance(value, int) or :
#             print(value)
#             total += float(value)
#     return total


# print(sum_summables(dictionary))


# items = [
#     {"a": 1, "b":2, "c": 3},
#     {"a":3, "size": 4},
#     {"b": 5, "d": 7}
# ]
# fields = ["a", "b"]


# def just_these_fields(items, fields):
#     result = []
#     # loop over the things, do something with each one
#     for item in items:
#         filtered = {}
#         for key in item:
#             if key in fields:
#                 filtered[key] = item[key]
#         result.append(filtered)
#     return result

# print(just_these_fields(items, fields))


# items = [
#     {"color":"blue", "size":"small"},
#     {"color":"red", "size":"small"},
#     {"color":"purple", "size":"medium"},
#     {"color":"green", "size":"large"},
# ]
# filters = {
#     "color": "blue",
#     "size": "medium"
# }


# def only_items_with(items, filters):
#     result = []
#     # loop over the things, get the things you want
#     for item in items:
#         for key, value in filters.items():
#             if (key, value) in item.items():
#                 result.append(item)
#     return result


# print(only_items_with(items, filters))

def closest_to_zero(nums):
    lowest = min(nums, key=abs)
    for num in nums:
        if abs(lowest) == abs(num) and lowest < num:
            return num
    return lowest

print(closest_to_zero([-1,1]))
