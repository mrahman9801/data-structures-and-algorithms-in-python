# basket = [
#     'apples',
#     'grapes',
#     'peaches'
# ]
# simple idea
# linked list: apples --> grapes --> peaches

# with place in memory taken into account
# apples
# 8989 --> grapes
#           3423 --> peaches
#                     3412 --> null

# obj1 = {1: True}
# obj2 = obj1
# obj1[1] = 'Lomao'
# del obj1
# print(obj2)

# Building a Linked List

# class Node():

#     def __init__(self, data):
#         self.data = data
#         self.next = None


# class LinkedList() :

#     def __init__(self):
#         self.head = None
#         self.tail = None

#     def append(self, data):
#         new_node = Node(data)
#         if self.head is None:
#             self.head = new_node
#             self.tail = self.head
#             self.length = 1
#         else:
#             self.tail.next = new_node
#             self.tail = new_node
#             self.length += 1

#     def prepend(self, data):
#         new_node = Node(data)
#         new_node.next = self.head
#         self.head = new_node
#         self.length += 1

#     def insert(self, index, data):
#         new_node = Node(data)
#         i = 0
#         temp = self.head
#         if index >= self.length:
#             self.append(data)
#             return
#         if index == 0:
#             self.prepend(data)
#             return
#         while i < self.length:
#             if i == index - 1:
#                 temp.next, new_node.next = new_node, temp.next
#                 self.length += 1
#                 break
#             temp = temp.next
#             i += 1

#     def remove(self, index):
#         temp = self.head
#         i = 0
#         if index >= self.length:
#             print('Invalid index')
#         if index == 0:
#             self.head = self.head.next
#             self.length -= 1
#             return
#         while i < self.length:
#             if i == index - 1:
#                 temp.next = temp.next.next
#                 self.length -= 1
#                 break
#             i += 1
#             temp = temp.next

#     def printl(self):
#         temp = self.head
#         while temp is not None:
#             print(temp.data, end = ' ')
#             temp = temp.next
#         print()
#         print('Length = ' + str(self.length))

#     def reverse(self):
#         prev = None
#         self.tail = self.head
#         while self.head is not None:
#             temp = self.head
#             self.head = self.head.next
#             temp.next = prev
#             prev = temp
#         self.head = temp


# # Doubly Linked List


# class Node:
#     def __init__(self,data):
#         self.data = data
#         self.next = None
#         self.prev = None

# class DoublyLinkedList:
#     def __init__(self):
#         self.head = None
#         self.tail = None

#     def append(self,data):
#         new_node = Node(data)
#         if self.head == None:
#             self.head = new_node
#             self.tail = self.head
#             self.length = 1
#         else:
#             self.tail.next = new_node
#             new_node.prev = self.tail
#             self.tail = new_node
#             self.length += 1

#     def prepend(self,data):
#         new_node = Node(data)
#         new_node.next = self.head
#         self.head.prev = new_node
#         self.head = new_node
#         self.length += 1

#     def insert(self,index,data):
#         new_node = Node(data)
#         if index==0:
#             self.prepend(data)
#             return
#         if index >= self.length:
#             self.append(data)
#             return
#         else:
#             leader = self.traversetoindex(index - 1)
#             holder = leader.next
#             leader.next = new_node
#             new_node.next = holder
#             new_node.prev = leader
#             holder.prev = new_node
#             self.length+=1

#     def remove(self,index):
#         if index==0:
#             self.head=self.head.next
#             self.length-=1
#             return
#         if index == self.length-1:
#             self.tail = self.tail.prev
#             self.tail.next = None
#             self.length -= 1
#             return
#         leader = self.traversetoindex(index-1)
#         unwanted_node = leader.next
#         holder = unwanted_node.next
#         leader.next = holder
#         holder.prev = leader
#         self.length -= 1


#     def traversetoindex(self,index):
#         curr_node = self.head
#         i = 0
#         while i!= index:
#             curr_node = curr_node.next
#             i+=1
#         return curr_node

#     def printt(self):
#         temp = self.head
#         while temp != None:
#             print(temp.data , end = ' ')
#             temp = temp.next
#         print()
#         print('Length ' + str(self.length))


# d = DoublyLinkedList()
# d.append(10)
# d.append(5)
# d.append(6)
# d.prepend(1)
# d.insert(2,22)
# d.remove(3)
# d.printt()


# Reverse a linked list, assuming you already are set up with the
# nodes class premade. Iterative solution with two pointers
# O(n) time, O(1) space since we change the linked list in place

# def reversal():
#     prev = None
#     curr = self.head
#     while curr:  # while it's not None (not past the current tail)
#         temp = curr.next # avoid losing next value
#         curr.next = prev # make it point backwards
#         prev = curr # iterate upwards
#         curr = temp
#     return prev # because the last prev would become the new head

# Is there a cyclic linked list? O(n) for both
#
# def hasCycle():
#     curr = head
#     seen = set()
#     while curr:
#         if curr.next in seen:
#             return True
#         seen.add(curr.next)
#         curr = curr.next
#     return False

# O(n) time. O(1) space
# def hasCycle():
#     slow, fast = head, head
#     while fast and fast.next:
#         slow = slow.next
#         fast = fast.next.next
#         if slow == fast:
#             return True
#     return False

# merge two presorted linked lists in order:
# def mergeTwo():
#     urmom = Node()
#     tail = urmom
#     while l1 and l2:
#         if l1.val < l2.val:
#             tail.next = l1
#             l1 = l1.next
#         else:
#             tail.next = l2
#             l2 = l2.next
#     if l1:
#         tail.next = l1
#     elif l2:
#         tail.next = l2
#     return urmom.next

# you return urmom.next because the .next stores the real added values, otherwise just urmom
# also brings that dummy beginning you use. notice how the tail is original urmom because
# you haven't added antyhing yet and when you do, tail is reassigned. i think.
