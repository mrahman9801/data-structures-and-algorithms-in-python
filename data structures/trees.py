import heapq

class Node():
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinarySearchTree():
    def __init__(self):
        self.root = None

    def insert(self, data):
        new_node = Node(data)
        if self.root is None:
            self.root = new_node
            return
        else:
            curr = self.root
            while curr:
                # Left
                if data < curr.data:
                    if curr.left is None:
                        curr.left = new_node
                        return
                    else:
                        curr = curr.left
                # Right
                elif data > curr.data:
                    if curr.right is None:
                        curr.right = new_node
                        return
                    else:
                        curr = curr.right

    def lookup(self, data):
        curr = self.root
        while True:
            if curr is None:
                return False
            if curr.data == data:
                return True
            # left
            if data < curr.data:
                curr = curr.left
            # right
            else:
                curr = curr.right

    def remove(self, data):
        pass


# bst = BinarySearchTree()
# bst.insert(10)
# bst.insert(5)
# bst.insert(6)
# bst.insert(12)
# bst.insert(8)
# print(bst.lookup)


x = [5,2,8,1,6,7,4,9]
#Heapify method sorts the list , this is min heap
heapq.heapify(x)
print(x)
heapq.heappush(x,0)
print(x)
# remove and return lowest value
print(heapq.heappop(x))
print(x)
# Used to pop out lowest element and push an element in same time
print (heapq.heappushpop(x, 5))
print(x)
#Used to get n largest elements in heap
print(heapq.nlargest(4,x))
#Used to get n smallest elements in heap
print(heapq.nsmallest(4,x))


# implement a Trie, a prefix tree
class TrieNode():
    # an empty node at the beginning of the trie
    def __init__(self):
        self.is_word = False
        # children dictionary basically has a collection of all the next possible
        # children. it replaces .right and .left
        self.children = {}


class Trie():
    def __init__(self):
        # blank
        self.root = TrieNode()

    def add(self, word):
        # start from black top
        # level 0
        node = self.root
        for char in word:
            # check if it doesn't exist first, cuz else you can just go start to it
            if char not in node.children:
                # create a node for it
                # for the first run, it adds the first letter to level 1
                # eventually you'll have all 26 letters in level 1
                node.children[char] = TrieNode()
            # go to that node for that char
            node = node.children[char]
        # declare that the word exists, basically it's a psuedotree where the
        # root is the whole path to the word
        node.is_word = True

    def search(self, word):
        node = self.root
        for char in word:
            if char not in node.children:
                return False
            node = node.children[char]
        return True

    def startsWith(self, prefix):
        node = self.root
        for char in prefix:
            if char not in node.children:
                return False
            node = node.children[char]
        return True
