# class Stack():
#     '''Stack implementation with list'''
#     def __init__(self):
#         self.arr = []
#         self.length = 0

#     def __str__(self):
#         return str(self.__dict__)

#     def peek(self):
#         return self.arr[self.length-1]

#     def push(self, value):
#         self.arr.append(value)
#         self.length += 1

#     def pop(self):
#         popped_item = self.arr[self.length - 1]
#         del self.arr[self.length - 1]
#         self.length -= 1
#         return popped_item

# mystack = Stack()
# mystack.push('google')
# mystack.push('microsoft')
# mystack.push('facebook')
# mystack.push('apple')
# print(mystack)
# x = mystack.peek()
# print(x)
# mystack.pop()
# print(mystack)


class Node():
    def __init__(self, val):
        self.data = val
        self.next = None


class Stack():
    '''Stack with a linked list'''
    def __init__(self):
        self.top = None
        self.bottom = None
        self.length = 0

    def peek(self):
        return self.top.data

    def push(self, data):
        new_node = Node(data)
        if self.bottom is None:
            self.bottom = new_node
            self.top = new_node
        else:
            new_node.next = self.top
            self.top = new_node
        self.length += 1

    def pop(self):
        if not self.top:
            return None
        holder = self.top
        self.top = self.top.next
        self.length -= 1
        if self.length == 0:
            self.bottom = None
        return holder.data

    def printt(self):
        temp = self.top
        while temp:
            print(temp.data, end = ' -> ')
            temp = temp.next
        print()


mystack = Stack()
mystack.push('google')
mystack.push('microsoft')
mystack.push('facebook')
mystack.push('apple')
mystack.printt()
x = mystack.peek()
print(x)
y=mystack.pop()
print(y)
mystack.printt()
qw = mystack.peek()
print(qw)


class Queue():
    def __init__(self):
        self.first = None
        self.last = None
        self.length = 0

    def peek(self):
        return self.first.data

    def enqueue(self, data):
        new_node = Node(data)
        if self.length == 0:
            self.first = new_node
            self.last = new_node
            self.length += 1
        else:
            # attach before we declare or else the original .last is lost
            self.last.next = new_node
            self.last = new_node
            self.length += 1

    def dequeue(self):
        temp = self.first.next
        dequeued = self.first
        if temp is None:
            self.first = None
            self.length -= 1
            return f'{dequeued} was the only one in the queue'
        self.first.next = None
        self.first = temp
        self.length -= 1
        # cant we just say self.first = self.first.next ???

    def printt(self):
        temp = self.first
        while temp is not None:
            print(temp.data, end = ' -> ')
            temp = temp.next
        print()
        print(self.length)


# Queue implementation with a stack
# basically, first item on the right, last on the left
# class MyQueue():

#     def __init__(self):
#         self.arr = []
#     add to the left of the list with list concatenation
#     def push(self, x: int) -> None:
#         self.arr = [x] + self.arr

#     def pop(self) -> int:
#         return self.arr.pop()

#     def peek(self) -> int:
#         return self.arr[-1]

#     def empty(self) -> bool:
#         return len(self.arr) == 0


# Your MyQueue object will be instantiated and called as such:
# obj = MyQueue()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.peek()
# param_4 = obj.empty()
