# strings = ['a', 'b', 'c', 'd', 'c', 'c', 'c']

# print(strings[1])

# strings.append('e') # O(1)
# strings.pop() # O(1)

# strings.insert(0, 'x') # O(n)
# strings.pop(3)
# print(strings)
# print(strings.count('c'))

#Array native python methods :
#append() Adds an element at the end of the list
#clear() Removes all the elements from the list
#copy()	Returns a copy of the list
#count() Returns the number of elements with the specified value
#extend() Add the elements of a list (or any iterable), to the end of the current list
#index() Returns the index of the first element with the specified value
#insert() Adds an element at the specified position
#pop() Removes the element at the specified position
#remove() Removes the first item with the specified value
#reverse() Reverses the order of the list
#sort()	Sorts the list based on ASCII

# collections.deque is queue library
# popleft()
# appendleft()
# extendleft()

# class Array:
#     def __init__(self):
#         self.length = 0
#         self.data = {}

#     def __str__(self):
#         return str(self.__dict__)

#     def get(self, index):
#         return self.data[index]

#     def push(self, item):
# self.length serves as our index key with an initial of 0: whatever_value
#         self.data[self.length] = item
#         self.length+=1

#     def pop(self):
#         lastitem = self.data[self.length-1]
#         del self.data[self.length-1]
#         self.length-=1
#         return lastitem

#     def remove(self, index):
#         deleteditem = self.data[index]
#         for i in range(index, self.length-1):
#             self.data[i] = self.data[i+1]

#         del self.data[self.length-1]
#         self.length-=1
#         return deleteditem

# arry = Array()
# arry.push(1)
# arry.push(2)
# arry.push(3)
# arry.push(4)
# arry.push(5)
# print(arry)
# arry.pop()
# print(arry)
# arry.remove(2)
# print(arry)


# O(n) time because we use a for loop
# def reverse(stri):
#     backwards = []
#     for i in range(len(stri)-1, -1, -1):
#         backwards.append(stri[i])
#     return ''.join(backwards)


# print(reverse('Yo my name trey'))


# O(n) time but O(1) space,,, doesnt work right now because str indexes aren't reassignable
# def reverse(string):
#     # get highest and lowest indices (lowest is always 0)

#     lowest = 0
#     highest = len(string) - 1
#     # swap their values at given indices
#     while lowest < highest:
#         string[lowest] = string[highest]
#         string[highest] = string[lowest]
#         # make them go up and down for the next round until they are in the middle
#         lowest -= 1
#         highest += 1


# print(reverse('Yo my name trey'))

# the easiest way but not optimal for interview
# def merge_sorted_arrays(arr1, arr2):
#     new_arr = []
#     for i in arr1:
#         new_arr.append(i)
#     for i in arr2:
#         new_arr.append(i)
#     new_arr.sort()
#     return new_arr

# better for interviews
# creating a variable to represent an index (such as an index of 0) helps us
# to access the index of an interable at the current value of said variable
# if we increment the variable + or -, we can access all the indices of the
# iterable like they are a team
# def merge_sorted_array(arr1, arr2):
#     # edgy case
#     if len(arr1)==0 or len(arr2)==0:
#         return arr1 + arr2
#     my_list = []
#     i = 0
#     j = 0
#     # we want to increase i and j until they reach the last index of their partner arr
#     while i < len(arr1) and j < len(arr2):
#         # if one team gets a number in, then that teams i/j increases
#         if arr1[i] <= arr2[j]:
#             my_list.append(arr1[i])
#             i += 1
#         elif arr2[j] < arr1[i]:
#             my_list.append(arr2[j])
#             j += 1
#     return my_list


# print(merge_sorted_array([1,3,4,6,20], [2,3,4,5,6,9,11,76]))
# print(merge_sorted_array([], []))

# high runtime, low memory
# def containsDuplicate(nums):
#     # set lookups are usually O(n) because they
#     # get implemented like dictionaries
#     # they don't add an element if it already
#     # exists
#     my_set = set()
#     for i in nums:
#         if i in my_set:
#             return True
#         my_set.add(i)
#     return False

# even faster because we use a simple comparison of two lengths
# instead of looking for a match in set
# def containsDuplicate(nums):
#     new_nums = set(nums)
#     return len(new_nums) != len(nums)


# print(containsDuplicate([1,2,7,4,5,6,7]))
