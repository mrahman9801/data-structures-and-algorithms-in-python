class Graph():
    def __init__(self):
        self.numNodes = 0
        self.adjacentList = {}

    def __str__(self):
        return str(self.__dict__)

    def addVertex(self, node):
        self.numNodes += 1
        self.adjacentList[node] = []

    def addEdge(self, node1, node2):
        self.adjacentList[node1].append(node2)
        self.adjacentList[node2].append(node1)

    def showConnection(self):
        for vertex in self.adjacentList:
            print(f'{vertex} --> {self.adjacentList[vertex]}')

myGraph = Graph()
myGraph.addVertex(1)
myGraph.addVertex(2)
myGraph.addVertex(3)
myGraph.addVertex(4)
myGraph.addEdge(1,2)
myGraph.addEdge(1,3)
myGraph.addEdge(2,3)
myGraph.addEdge(3,4)
myGraph.showConnection()
