def addto80(n):
    return n + 8

# cache = {}
# def memoizedAddto80(n):
#     if n in cache:
#         return cache[n]
#     cache[n] = n + 80
#     return cache[n]

# def memoizedadd80():
#   cache = {}

#   def memoized(n):
#     if n in cache:
#         return cache[n]
#     else:
#         print('Long time')
#         cache[n] = n+80
#         return cache[n]
#   return memoized

# memo = memoizedadd80()
# print(memo(7))
# print(memo(7))


def memoFib():
    cache = {}

    def fib(n):
        if n in cache:
            return cache[n]
        elif n < 2:
            cache[n] = n
            return cache[n]
        else:
            cache[n] = fib(n-1) + fib(n-2)
            return cache[n]
    return fib


fib = memoFib()
print(fib(998))

# cache = {}

# def fibo(n):
#   if n in cache:
#     return cache[n]
#   elif n < 2:
#     cache[n] = n
#     return cache[n]
#   else :
#     cache[n] = fib(n-1) + fib(n-2)

#     return cache[n]


# print(fibo(5))
