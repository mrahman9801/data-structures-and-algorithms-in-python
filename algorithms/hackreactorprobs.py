# def filter_available_squares(candidate_squares, rook):
#     available_squares = []
#     for square in candidate_squares:
#         # check columns
#         if square[1] != rook[1]:
#             # check rows
#             if square[0] != rook[0]:
#                 available_squares.append(square)
#     return available_squares


# N = 8
# rooks = []
# available_squares = [(r,c) for r in range(N) for c in range(N)]
# # print(available_squares)
# # stop here
# while len(available_squares) > 0:
#     # get rook from avail squares
#     rook = available_squares[0]
#     # add rook to list of rooks
#     rooks.append(rook)
#     # elim squares
#     available_squares = filter_available_squares(available_squares, rook)
# print(rooks)


# def reverse_prefix(s, letter):
#     prefix = ''
#     counter = 0
#     for i in s:
#         prefix += i
#         counter += 1
#         if i == letter:
#             break
#     rev_pre = prefix[::-1]
#     new_word = ''
#     for index, i in enumerate(s):
#         if index < counter:
#             new_word += rev_pre[index]
#         else:
#             new_word += i
#     return new_word

# print(reverse_prefix('abcd', 'a'))


# def parse_calculation(s):
#     ops = s.split(" ")
#     return eval(f'{ops[1]} {ops[0]} {ops[2]}')


# print(parse_calculation("/ 10 5"))


# def parse_calculation_ext(s):
#     if len(s) == 0:
#         return s
#     strip = helper(s)
#     return parse_calculation_ext(strip)

# def helper(s):
#     strip = s.strip("( )")
#     return strip

# print(parse_calculation_ext("(- (+ (+ 2 4 ) (* 1 8 ) ) 15 )"))

# import re

# def calc_match(match):
#     s = match.group()
#     parts = s.split(' ')
#     return str(eval(f'{parts[1]} {parts[0][1]} {parts[2]}'))

# def calculate(s):
#     while "(" in s:
#         s = re.sub('\([+-/*] \d+ \d+ \)', calc_match, s)
#     return s

# long = "(- (+ (+ 2 4 ) (* 1 8 ) ) 15 )"
# print(calculate(long))


# def make_integer(nums, length):
#     smallest = ''
#     nums.sort(reverse=True)
#     while len(smallest) < length:
#         tiny = nums.pop()
#         smallest += str(tiny)
#     return int(smallest)

# print(make_integer([3, 1, 2, 6, 5, 9], 5))






# def does_product_exist(nums, left, right, target):
#     i = left
#     j = i + 1
#     divs = set()
#     divs.add(target/nums[i])
#     while i < right:
#         if nums[j] in divs:
#             return True
#         divs.add(target/nums[j])
#         i+=1
#         j+=1
#     return False


# print(does_product_exist([2,4,2,4,2,4], 0, 3, 16))

# def fix_misspellings(corrections):
#     output = []
#     for item in corrections:
#         word = item["word"]
#         fixed = word[:item["position"]-1] + word[item["position"]:]
#         print(fixed)
#         output.append(fixed)
#     return output


# print(fix_misspellings([
#     { "word": "tablett", "position": 7 },
#     { "word": "marrble", "position": 4 },
#     { "word": "xdocker", "position": 1 },
#     ]))


# def find_odd(nums):
#     mydict = {}
#     for num in nums:
#         mydict[num] = mydict.get(num, 0) + 1
#     for prop in mydict:
#         if mydict[prop] % 2 != 0:
#             return prop



# print(find_odd([3, 4, 4, 3, 4, 18, 4]))

# def digits_to_number(digits):
#     s = ''
#     for num in digits:
#         s += str(num)
#     s = s[::-1]
#     return int(s)

# def sum_reversed_digits_as_list(reversed_digits1, reversed_digits2):
#     total_str = str(digits_to_number(reversed_digits1) + digits_to_number(reversed_digits2))
#     out = [int(char) for char in total_str]
#     return out


# print(sum_reversed_digits_as_list([3], [3, 2, 1]))


# def int_to_roman(int):
#     romans = {
#         1: 'I',
#         4: 'IV',
#         5: 'V',
#         9: 'IX',
#         10: 'X',
#         40: 'XL',
#         50: 'L',
#         90: 'XC',
#         100: 'C',
#         400: 'CD',
#         500: 'D',
#         900: 'CM',
#         1000: 'M'
#     }

#     keys = [1000,900,500,400,100,90,50,40,10,9,5,4,1]
#     strings = []
#     index = 0
#     while int > 0:
#         if int >= keys[index]:
#             strings.append(romans[keys[index]])
#             int -= keys[index]
#         else:
#             index += 1
#     return "".join(strings)
#     # return strings

# print(int_to_roman(2023))

# def pipe_outputs(num_pipes, steps):
#     current = []
#     for num in range(num_pipes):
#         current.append(8)
#     for step in steps:
#         if len(step) == 2:
#             if step[0] == 1:
#                 holder = current[0]
#                 del current[0]
#                 current.insert(0, step[1])
#                 current.insert(1, holder-step[1])
#             else:
#                 current.insert(step[0]-1, step[1])
#                 current.insert(step[0]+1, current[step[0]]-step[1])
#                 del current[step[0]]
#         else:
#             pipe_index = step[0] - 1
#             current[pipe_index] = current[pipe_index] + current[pipe_index+1]
#             del current[pipe_index+1]
#     return current


# print(pipe_outputs(3, [[2, 4], [1], [1], [1, 2]]))

# def solution(A):
#     # write your code in Python 3.8.10
#     # If all nums are negative
#     highest_num = max(A)
#     if highest_num < 1:
#         return 1
#     # If only one num in A
#     if A[-1] == 1:
#         return 2
#     # Else
#     length = len(A)
#     my_set = set(A)
#     # Must put + 2 to add the next number if all numbers are consistently consecutive
#     for i in range(1, length+2):
#         if i not in my_set:
#             return i


# print(solution([-1,-3]))

# from datetime import datetime
# def solution(A, B):
#     # write your code in Python 3.8.10
#     start = datetime.strptime(A, "%H:%M")
#     end = datetime.strptime(B, "%H:%M")
#     diff = end - start
#     seconds = diff.total_seconds()
#     minutes = seconds / 60
#     return minutes % 15


# print(solution("12:01", "12:44"))

# def has_bit_parity(a_number):
#     # Write your code here
#     bits = str(bin(a_number))
#     count = 0
#     for i in bits:
#         if i == '1':
#             count += 1
#     if count % 2 != 2:
#         return 1

# print(has_bit_parity(-34))

# def isPangram(input):
#     out = ''
#     for i in input:
#         i = i.lower()
#         curr = set(i)
#         letters = [char for char in curr if char.isalpha()]
#         if len(letters) == 26:
#             out += '1'
#         else:
#             out += '0'
#     return out


# print(isPangram(['pack my box with five dozen liquor jugs', 'this is not a pangram']))

# def missingWords(s, t):
#     full = s.split()
#     part = t.split()
#     missing = []
#     i = 0
#     j = 0




# print(missingWords('I like cheese', 'like'))


# def findMinimumDays(durations):
#     # Write your code here
#     days = 0
#     highest = 0
#     for i in range(len(durations)-2):
#         for j in range(1,len(durations)-1):
#             current = durations[i] + durations[j]
#             if current <= 3:
#                 if current > highest:
#                     highest = current





#     return days


# print(findMinimumDays([1.90, 1.04, 1.25, 2.5, 1.75]))

def count_depth_increases(depths):
    increases = 0
    for i in range(len(depths)-3):
        current = depths[i] + depths[i+1] + depths[i+2]
        next = current - depths[i] + depths[i+3]
        if next > current:
            increases += 1
    return increases


print(count_depth_increases([199,200,208,210,200,207,240,269,260,263]))
