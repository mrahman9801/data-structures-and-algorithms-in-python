# lst = [5,6,2,4,0,1,4]
# Timsort O(n*log(n))
# lst.sort()
# print(lst)

# Pretty bad, used in education
# def BubbleSort(lst):
#     n = len(lst)
#     # n-1 lets us avoid an extra unnecessary loop, just n will work too
#     for i in range(n-1):
#         # - i because the last i elements already swapped, just n-1 will work too
#         for j in range(n-i-1):
#             if lst[j] > lst[j+1]:
#                 lst[j], lst[j+1] = lst[j+1], lst[j]
#     return lst

# print(BubbleSort([5,9,1,2,7,11,3,8,2]))

# Not efficient, education
# def SelectionSort(lst):
#     n = len(lst)
#     for i in range(n):
#         # Sees first index so it thinks its the min so far
#         min_index = i
#         for j in range(n-1):
#             if lst[j] < lst[min_index]:
#                 # Reassigns min index
#                 min_index = j
#             lst[j], lst[min_index] = lst[min_index], lst[j]
#     return lst

# print(SelectionSort([99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0]))


# Best for minimal data and nearly sorted data
# def InsertionSort(lst):
#     length = len(lst)
#     i = 1
#     end = lst[0]
#     while i < length:
#         # compare adjacent values
#         if lst[i] < end:
#             val = lst.pop(i)
#             # if moved value is less than the first j value seen,
#             # insert it at position j, then immediately break
#             for j in range(i):
#                 if val < lst[j]:
#                     lst.insert(j, val)
#                     break
#         end = lst[i]
#         i += 1
#     return lst

# print(InsertionSort([99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0]))

# Good for divide and conquer, worst is still O(nlog(n)), O(n) space
# def merge_sort(values, left=None, right=None):
#     if left is None and right is None:
#         left = 0
#         right = len(values) - 1
#     # Base case
#     if left >= right:
#         return
#     # Recursive cases
#     # Find the middle to split
#     middle = (right + left) // 2
#     # Sort the left half
#     merge_sort(values, left, middle)
#     # Sort the right half
#     merge_sort(values, middle + 1, right)
#     # Merge them together
#     merge(values, left, middle, right)
#     return values

# def merge(values, left, middle, right):
#     right_start = middle + 1
#     # Terminal case to make sure we don't loop forever
#     if values[middle] <= values[right_start]:
#         return
#     # Merge the sub-lists by looping and comparing
#     # the values at the start of each list
#     while left <= middle and right_start <= right:
#         # The one on the left is less than the
#         # one on the right, so just keep going
#         if values[left] <= values[right_start]:
#             left += 1
#         else:
#             # In this case, the one on the right half
#             # is less than one in the left half, so
#             # we need to swap the values
#             value = values[right_start]
#             index = right_start
#             # Move the all of the values to the right
#             # by one
#             while index != left:
#                 values[index] = values[index - 1]
#                 index -= 1
#             # Put the value into the new "empty" place
#             values[left] = value
#             # Increment all of the indexes
#             left += 1
#             middle += 1
#             right_start += 1



# print(merge_sort([324324,323,4656,0,21424214,21,45353,1111,2323,555,69,420]))


# Good when pivot isn't highest or lowest value, if it's not when it could be O(n^2)
# else it's o(nlog(n)) and the fastest, O(log(n)) space
# divide and conquer
# doesn't do unneccessary element swaps
# def partition(values, left, right):
#     pivot = values[right]
#     swap = left - 1
#     for i in range(left, right):
#         if values[i] <= pivot:
#             # swap will only go up if it needs to
#             swap += 1
#             values[swap], values[i] = values[i], values[swap]
#     # swap increments 1 last time
#     swap += 1
#     values[swap], values[right] = values[right], values[swap]
#     return swap

# def quicksort(values, left=None, right=None):
#     if left is None and right is None:
#         left = 0
#         right = len(values) - 1
#     if left >= right or left < 0:
#         return
#     # p will be a swap value
#     p = partition(values, left, right)
#     quicksort(values, left, p - 1)
#     quicksort(values, p + 1, right)
#     return values

# print(quicksort([324324,323,4656,0,21424214,21,45353,1111,2323,555,69,420]))


# Heapsort on average worse than quicksort, but worst speed is O(nlog(n)) and space O(n)
# swaps even if sorted, so quicksort seen as better


# A non-comparison sort that skips comparing values and uses buckets instead,
# this only works with numbers. The sort for strings is bucket sort
def radix_sort(values):
    # Find the maximum value in the input array
    max_item = max(values)
    # Find the number of digits in the largest value
    digit_count = 1
    while max_item > 0:
        max_item /= 10
        digit_count += 1
    # Start with the rightmost digit for sorting
    tens_place = 1
    # Step 4
    while digit_count > 0:
        # Ten buckets because 10 possible digits
        buckets = [0] * 10
        n = len(values)
        for i in range(n):
            # Go through each value in values and put
            # note that it's in the proper bucket
            sort_digit = (values[i] // tens_place) % 10
            buckets[sort_digit] += 1
        # Do some fancy math so that we know which
        # numbers are in which bucket
        for i in range(1, 10):
            buckets[i] += buckets[i-1]
        # Move the values from the buckets into
        # another list
        sorted_values = [0] * n
        i = n - 1
        while i >= 0:
            item = values[i]
            sort_digit = (values[i] // tens_place) % 10
            buckets[sort_digit] -= 1
            sorted_position = buckets[sort_digit]
            sorted_values[sorted_position] = item
            i -= 1
        # Put the values from the sorted list
        # back into the origin values list
        for i, item in enumerate(sorted_values):
            values[i] = item
        tens_place *= 10
        digit_count -= 1


# sorting 10 schools by distance: maybe insertion sort
# Sort ebay listings by current bid amount: radix sort
# ESPN scores: Quick sort, don't know if the pivot could be radical outlier
# Massive database and sort thru past year's user data: merge sort
# Almost sorted review data needs to update and add 2 new reviews: insertion sort
# Temperature records of Canada past 50 years: quicksort if decimals, else radix
# Username database, very random: bucketsort, or quicksort
# Teaching sorting: bubble sort

# Stability: if two values in a list being sorted both satisfy
# the sorting key (maybe the first letter being "s" in both words),
# then the value that is first in the list will be put before the
# second value
# Unstable: Quicksort, heapsort, selection sort
# Stable:Timsort, mergesort, insertion sort, bubble sort
