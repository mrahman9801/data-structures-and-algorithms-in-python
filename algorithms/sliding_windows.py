# 121. this is the two pointer way: super slow
# def maxProfit(prices):
#     top = 0
#     current = 0
#     for i in range(len(prices)):
#         end = len(prices) - 1
#         while i < end:
#             current = prices[end] - prices[i]
#             top = max(current, top)
#             end -= 1
#     if top > 0:
#         return top
#     else:
#         return 0

# find the sliding window method

# 3 Longest Substring without repeating characters

def lengthOfLongest(s):
    start = 0
    end = 0
    longest = 0
    # can also use set so it looks nicer
    keys = {}
    # while the indices are valid
    while start < len(s) and end < len(s):
        if s[end] not in keys:
            keys[s[end]] = s[end]
            # how big can you make it
            longest = max(longest, end-start+1)
            end += 1
        else:
            # it will start deleting all items in keys up to that
            # repeating character and start again from that character
            del keys[s[start]]
            start += 1
    return longest


print(lengthOfLongest("abcabcbb"))
