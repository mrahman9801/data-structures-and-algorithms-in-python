def threeSum(nums):
    nums.sort()
    output = set()
    target = 0
    for i in range(len(nums)):
        start = i + 1
        end = len(nums) - 1
        while start < end:
            total = nums[i] + nums[start] + nums[end]
            if total == target:
                output.add((nums[i], nums[start], nums[end]))
                start += 1
                end -= 1
            elif total < target:
                start +=1
            elif total > target:
                end -= 1
    return output
