def numIslands(self, grid: List[List[str]]) -> int:
        def search(grid, i, j):
            # out of bounds
            if (i < 0 or i >= len(grid) or j < 0 or j >= len(grid[0])):
                return
            # found water
            if grid[i][j] == '0':
                return
            # turn land into water to prevent over counting
            grid[i][j] = '0'
            # spread out to find nearby land
            search(grid, i-1, j)
            search(grid, i+1, j)
            search(grid, i, j-1)
            search(grid, i, j+1)

        # matrix traversal always requires nested loops
        rows = len(grid)
        columns = len(grid[0])
        num_islands = 0
        for i in range(rows):
            for j in range(columns):
                if grid[i][j] == '1':
                    num_islands += 1
                    search(grid, i, j)

        return num_islands


def islandPerimeter(self, grid: List[List[int]]) -> int:
        def search(grid, visited, i, j):
            # if wall boundary
            if i < 0 or i >= len(grid) or j < 0 or j >= len(grid[0]):
                return 1
            # if water boundary
            if grid[i][j] == 0:
                return 1
            # if already visited
            if visited[i][j]:
                return 0
            visited[i][j] = True
            perimeter = 0
            perimeter += search(grid, visited, i+1, j)
            perimeter += search(grid, visited, i-1, j)
            perimeter += search(grid, visited, i, j+1)
            perimeter += search(grid, visited, i, j-1)
            return perimeter

        rows = len(grid)
        columns = len(grid[0])
        # makes sure land isn't revisited in recursion
        visited = [[False for i in range(columns)] for j in range(rows)]

        for i in range(rows):
            for j in range(columns):
                if grid[i][j] == 1 and not visited[i][j]:
                    return search(grid, visited, i, j)
