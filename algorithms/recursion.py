def recursiveFact(num):
    if num == 1:
        return 1
    return num * recursiveFact(num-1)


def iterativeFact(num):
    out = 1
    while num > 0:
        out *= num
        num -= 1
    return out

# print(iterativeFact(3))
# print(recursiveFact(4))
def fibonacciRecursive(index):
    if index < 2:
        return index
    return fibonacciRecursive(index-1) + fibonacciRecursive(index-2)

# print(fibonacciRecursive(99)) will kill computer lol because its O(2^n)

# O(n)
def fibonacciIterative(index):
    if index < 2:
        return index
    a = 0
    b = 1
    total = 0
    for i in range(index-1):
        total = a + b
        a = b
        b = total
    return total

# print(fibonacciIterative(999))


def reverseString(string):
    if len(string) == 0:
        return string
    # print(f'{reverseString(string[1:]) + string[0]}')
    return reverseString(string[1:]) + string[0]

print(reverseString("no more chicken"))
