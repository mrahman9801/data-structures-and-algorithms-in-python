# Input as a sorted list
# def BinarySearch(input, target):
#     start = 0
#     end = len(input) - 1
#     if start > end:
#         return 'Empty list'
#     need to put equal in case of 1 item in list
#     while start <= end:
#         half = (start+end)//2
#         val = input[half]
#         if val == target:
#             return f'{val} at index {input.index(val)}'
#         elif val > target:
#             end = half - 1
#         else:
#             start = half + 1
#     return 'Target not in list'


# print(BinarySearch([1,3,5,6,7,8,123,4444,9999], 3))

# BFS
def BFS(input, target):
    queue = [input]
    while len(queue) > 0:
        curr = queue.pop(0)
        if curr.val == target:
            return curr
        if curr.left:
            queue.append(curr.left)
        if curr.right:
            queue.append(curr.right)
    return None
    # return none is implied actually

# DFS pre-order (looks like the tree itself if you make it return a list and not the node answer)
def DFS(input, target):
    stack = [input]
    while len(stack) > 0:
        curr = stack.pop()
        if curr.val == target:
            return curr
        if curr.left:
            stack.append(curr.left)
        if curr.right:
            stack.append(curr.right)
    return None

# If solution not far from root of tree: BFS
# If tree very deep and solutions are rare: BFS (DFS takes too long)
# If tree is very wide: DFS (BFS takes too much memory)
# If solutions are frequent but deep in tree: DFS
# Determining whether path exists between nodes: DFS
# Finding shortest path: BFS

# DFS Recursive InOrder (returns a list of all nodes in this example)
def DFSInOrder(input, list=[]):
    if input.left:
        DFSInOrder(input.left, list)
    list.append(input.value)
    if input.right:
        DFSInOrder(input.right, list)
    return list
